TARGET: sim_shell

CC = gcc
CFLAGS = -Wall -g
OBJ = sim_shell.o

all: sim_shell

sim_shell: $(OBJ)
	$(CC) $(CFLAGS) -o sim_shell $(OBJ)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm sim_shell
	rm sim_shell.o
